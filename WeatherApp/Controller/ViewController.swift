////  ViewController.swift
//  WeatherApp
//
//  Created on 29/09/2020.
//  
//

import UIKit
import CoreLocation

class ViewController: UIViewController  {

    @IBOutlet var tableView: UITableView!
    
    var models = [DailyWeatherEntry]()
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    
    var current: CurrentWeather?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(WeatherTableViewCell.nib(), forCellReuseIdentifier: WeatherTableViewCell.identyfier)
        tableView.register(HourlyTableViewCell.nib(), forCellReuseIdentifier: HourlyTableViewCell.identyfier)
    
        setupLocation()
    }
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: WeatherTableViewCell.identyfier, for: indexPath) as! WeatherTableViewCell
        cell.configure(with: models[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    
}

extension ViewController : CLLocationManagerDelegate {
    
    func setupLocation() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !locations.isEmpty, currentLocation == nil {
            currentLocation = locations.first
            locationManager.stopUpdatingLocation()
            
            requestWeatherForLocation()
        }
    }
    
    func requestWeatherForLocation() {
        guard let currentLocation = currentLocation else {
            return
        }
        let long = currentLocation.coordinate.longitude
        let lat  = currentLocation.coordinate.latitude
        
        let urlString = "https://api.darksky.net/forecast/ddcc4ebb2a7c9930b90d9e59bda0ba7a/\(lat),\(long)?exclude=[flags,minutely]"
        
        URLSession.shared.dataTask(with: URL(string: urlString)!) { (data, response, err) in
            
            // Validation
            if let err = err {
                print("Failed to fetch courses:", err)
                return
            }
            guard let data = data else { return }
            
            var json: WeatherResponse?
            
            do {
                json = try JSONDecoder().decode(WeatherResponse.self, from: data)
                
            } catch let error {
                print("Failed to decode: \(error)")
            }
            
            guard let result = json else { return }
            
            // (dodanie tablicy pomiarow dziennych)
            let entries = result.daily.data
            self.models.append(contentsOf: entries)
            
            let current = result.currently
            self.current = current
            
            print(entries)
            DispatchQueue.main.async {
                self.tableView.reloadData()
                
                // header
                self.tableView.tableHeaderView = self.createtableHeader()
            }
            
        }.resume()
        
        print("long \(long) | lat\(lat)")
    }
    
    func createtableHeader() -> UIView {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.width))
        
        headerView.backgroundColor = UIColor(red: 100/255, green: 109/255, blue: 179/255, alpha: 1)
        
        let locationLabel = UILabel(frame: CGRect(x: 10, y: 10, width: view.frame.size.width - 20, height: headerView.frame.size.height / 5))
        let summaryLabel = UILabel(frame: CGRect(x: 10, y: 20 + locationLabel.frame.size.height , width: view.frame.size.width - 20, height: headerView.frame.size.height / 5))
        let tempLabel = UILabel(frame: CGRect(x: 10, y: 20 + locationLabel.frame.size.height + summaryLabel.frame.size.height, width: view.frame.size.width - 20, height: headerView.frame.size.height / 2))
        
        headerView.addSubview(locationLabel)
        headerView.addSubview(summaryLabel)
        headerView.addSubview(tempLabel)
        
        tempLabel.textAlignment = .center
        summaryLabel.textAlignment = .center
        locationLabel.textAlignment = .center
        
        tempLabel.font = UIFont(name: "Helvetica-Bold", size: 32)
        
        guard let currentWeather = self.current else {
            return UIView()
        }
        
        tempLabel.text = "\(currentWeather.temperature)"
        summaryLabel.text = "\(currentWeather.summary)"
        tempLabel.text = "Clear"
        
        return headerView
    }
}


