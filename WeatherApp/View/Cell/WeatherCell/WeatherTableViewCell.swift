////  WeatherTableViewCell.swift
//  WeatherApp
//
//  Created on 29/09/2020.
//  
//

import UIKit

class WeatherTableViewCell: UITableViewCell {

    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var highTempLabel: UILabel!
    @IBOutlet var lowTempLabel: UILabel!
    @IBOutlet var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .systemGray5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static let identyfier = "WeatherTableViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "WeatherTableViewCell", bundle: nil)
    }
    
    func configure (with model: DailyWeatherEntry) {
        
        self.lowTempLabel.textAlignment = .center
        self.highTempLabel.textAlignment = .center
        
        self.lowTempLabel.text = ("\(Int(model.temperatureLow))°")
        self.highTempLabel.text = ("\(Int(model.temperatureHigh))°")
        self.dayLabel.text = getDayForDate(Date(timeIntervalSince1970: Double(model.time)))
        
        let icon = model.icon.lowercased()
        if icon.contains("clear") {
            self.iconImageView.image = UIImage(systemName: "sun.max")
        } else if icon.contains("rain") {
            self.iconImageView.image = UIImage(systemName: "cloud.rain")
        } else {
            self.iconImageView.image = UIImage(systemName: "cloud")
        }
        
        self.iconImageView.contentMode = .scaleAspectFit
    }
    
    func getDayForDate(_ date: Date?) -> String {
        guard let inputDate = date else {
            return ""
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        return formatter.string(from: inputDate)
    }
}
